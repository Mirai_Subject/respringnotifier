#import <UIKit/UIkit.h>

%hook SpringBoard
-(void)applicationDidFinishLaunching:(id)application {
    %orig;
    UIAlertView *alert = [[UIAlertView alloc] 
initWithTitle:@"RespringNotifier by Subject4S" 
        message:@"Respring Complete! Welcome Back!" 
        delegate:nil 
        cancelButtonTitle:@"Dismiss" 
        otherButtonTitles:nil];
    [alert show];
    [alert release];
}
%end

